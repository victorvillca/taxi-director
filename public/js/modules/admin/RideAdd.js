/**
 * Javascript for Taxi Director.
 *
 * @category Taxi
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 Leadersof A/S
 * @license Proprietary
 */

var com = com || {};
com.em = com.em ||{};
	com.em.RideAdd = function () {
		// form to create and update
		this.dialogForm;
		// For the show messages
		this.alert;
		this.validator;
		this.initFlashMessage();
	};
com.em.RideAdd.prototype = {

	/**
	 * Initializes JQuery flash message component
	 */	
	initFlashMessage: function() {
		this.alert = new com.em.Alert();
	},

	/**
	 * Settings the form
	 * @param selector (dialog of form)	 
	 * */
	configureDialogForm: function(selector) {with (this) {
		dialogForm = $(selector).dialog({
			autoOpen: false,
			height: 440,
			width: 263,
			modal: true,
			close: function(event, ui) {
				$(this).remove();
			}
		});

		// Configs font-size for header dialog and buttons
		$(selector).parent().css('font-size','0.7em');
	}},

	/**
	 * Opens form dialog to manages the creation of new register
	 * @param selector
	 */
	clickToAdd: function(selector) {with (this) {
		$(selector).bind('click',function(event) {
			event.preventDefault();

			var action = $(this).attr('href');

			$.ajax({
				url: action ,
				type: "GET",
				beforeSend : function(XMLHttpRequest) {

				},

				success: function(data, textStatus, XMLHttpRequest) {
					if (textStatus == 'success') {
						var contentType = XMLHttpRequest.getResponseHeader('Content-Type');
						if (contentType == 'application/json') {
							alert.show(data.message, {header: com.em.Alert.FAILURE});
						} else {
							// Getting html dialog
							$('#dialog').html(data);
							// Configs dialog
							configureDialogForm('#dialog-form', 'insert');
							// Sets validator
							setValidatorForm("#formId");
							// Opens dialog
							dialogForm.dialog('open');
							// Loads buttons for dialog. dialogButtons is defined by ajax
							dialogForm.dialog( "option" , 'buttons', dialogButtons);
						}
					} 
				},

				complete: function(jqXHR, textStatus) {
				},

				error: function(jqXHR, textStatus, errorThrown) {
					dialogForm.dialog('close');
					alert.flashError(errorThrown,{header : com.em.Alert.ERROR});
				}
			});
		});
	}},

	/**
	 * Validates Ride form
	 * @param selector
	 */
	setValidatorForm : function(selector) {
		validator = $(selector).validate({
			rules:{
				'phone':{
					maxlength: 15,
					number: true,
					required: true
				}
			},
			messages: {
				'phone': {
					maxlength: 'Por favor, introduzca un maximo de 15 caracteres.',
					number: 'Por favor introduzca un numero',
					required: 'Este campo es obligatorio'
				}
			}
		});
	},

	/**
	 * Shows alert if it exists, if it not create a new instance of alert and show it
	 * @param message to show
	 * @param header of the message
	 */
	showAlert: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.show(message, header);
	}},

	/**
	 * Shows flash message success if it exists, if it not creates a new instance of flash message success and shows it.
	 * @param message string
	 * @param header string
	 */
	flashSuccess: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.flashSuccess(message, header);
	}},

	/**
	 * Shows flash message error if it exists, if it not creates a new instance of flash message error and shows it.
	 * @param message string
	 * @param header string
	 */
	flashError: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.flashError(message, header);
	}}
};