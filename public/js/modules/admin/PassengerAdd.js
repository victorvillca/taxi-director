/**
 * Javascript for Taxi Director.
 *
 * @category Taxi
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 Leadersof A/S
 * @license Proprietary
 */

var com = com || {};
com.em = com.em ||{};
	com.em.PassengerAdd = function () {
		// For create or update register
		this.dialogForm;
		// For show message to client
		this.alert;
		// For data table
		this.table;
		// urls
		this.url = {};
		this.validator;
		this.validatorSearch;

		this.initFlashMessage();

		this.dtHeaders = undefined;
	};
com.em.PassengerAdd.prototype = {

	/**
	 * Initializes JQuery flash message component
	 */	
	initFlashMessage: function() {
		this.alert = new com.em.Alert();
	},

	/**
	 * Configures the form
	 * @param selector (dialog of form)	 
	 * */
	configureDialogForm: function(selector) {with (this) {
		dialogForm = $(selector).dialog({
			autoOpen: false,
			height: 150,
			width: 310,
			modal: true,
			close: function(event, ui) {
				$(this).remove();
			}
		});

		$(selector).parent().css('font-size','0.7em');
	}},

	/**
	 * Configures the form
	 * @param selector (dialog of form)	 
	 * */
	configureDialogAddForm: function(selector) {with (this) {
		dialogForm = $(selector).dialog({
			autoOpen: false,
			height: 300,
			width: 210,
			modal: true,
			close: function(event, ui) {
				$(this).remove();
			}
		});

		$(selector).parent().css('font-size','0.7em');
	}},

	/**
	 * Opens dialog and manages the creation of new register
	 * @param selector
	 */
	clickToAdd: function(selector) {with (this) {
		$(selector).bind('click',function(event) {
			event.preventDefault();

			var action = $(this).attr('href');

			$.ajax({
				url: action ,
				type: "GET",
				beforeSend : function(XMLHttpRequest) {},

				success: function(data, textStatus, XMLHttpRequest) {
					if (textStatus == 'success') {
						var contentType = XMLHttpRequest.getResponseHeader('Content-Type');
						if (contentType == 'application/json') {
							alert.show(data.message, {header: com.em.Alert.FAILURE});
						} else {
							// Getting html dialog
							$('#dialog').html(data);
							// Configs dialog
							configureDialogAddForm('#dialog-form', 'insert');
							// Sets validator
							setValidatorForm("#formId");
							// Opens dialog
							dialogForm.dialog('open');
							// Loads buttons for dialog. dialogButtons is defined by ajax
							dialogForm.dialog( "option" , 'buttons', dialogButtons);
						}
					} 
				},

				complete: function(jqXHR, textStatus) {},

				error: function(jqXHR, textStatus, errorThrown) {
					dialogForm.dialog('close');
					alert.flashError(errorThrown,{header : com.em.Alert.ERROR});
				}
			});
		});
	}},

	/**
	 * Opens dialog and manages the creation of new register
	 * @param selector
	 */
	clickToSearch: function(selector) {with (this) {
		$(selector).bind('click',function(event) {
			event.preventDefault();

			var action = $(this).attr('href');

			$.ajax({
				url: action ,
				type: "GET",
				beforeSend : function(XMLHttpRequest) {},

				success: function(data, textStatus, XMLHttpRequest) {
					if (textStatus == 'success') {
						var contentType = XMLHttpRequest.getResponseHeader('Content-Type');
						if (contentType == 'application/json') {
							alert.show(data.message, {header: com.em.Alert.FAILURE});
						} else {
							// Getting html dialog
							$('#dialog').html(data);
							// Configs dialog
							configureDialogForm('#dialog-form', 'insert');
							// Sets validator
							setValidatorSearchForm("#formId");
							// Opens dialog
							dialogForm.dialog('open');
							// Loads buttons for dialog. dialogButtons is defined by ajax
							dialogForm.dialog( "option" , 'buttons', dialogButtons);
						}
					} 
				},

				complete: function(jqXHR, textStatus) {},

				error: function(jqXHR, textStatus, errorThrown) {
					dialogForm.dialog('close');
					alert.flashError(errorThrown,{header : com.em.Alert.ERROR});
				}
			});
		});
	}},

	/**
	 * Configures the name autocomplete of the filter
	 * @param selector
	 */
	configureLabelAuto: function(selector) { with (this) {
		$(selector).autocomplete({
			source: function(request, response) {
				$.ajax({
					url: url.toAutocompleteLabel,
					dataType: 'json',
					data: {name_auto: request.term},
					success: function(data, textStatus, XMLHttpRequest) {
						response($.map(data.items, function(item) {
							return {
								label: item
							};
						}));
					}
				});
			}
		});
	}},

	/**
	 * Validates Passenger form
	 * @param selector
	 */
	setValidatorForm : function(selector) {
		validator = $(selector).validate({
			rules:{
				'phone':{
					maxlength: 15,
					number: true,
					required: true
				},
				'firstName':{
					maxlength: 100,
					required: true
				}
			},
			messages:{
				'phone': {
					maxlength: 'Por favor, introduzca un maximo de 15 caracteres.',
					number: 'Por favor introduzca un numero',
					required: 'Este campo es obligatorio'
				},
				'firstName': {
					maxlength: 'Por favor, introduzca un maximo de 100 caracteres.',
					required: 'Este campo es obligatorio'
				}
			}
		});
	},

	/**
	 * Validates Search Passenger form
	 * @param selector
	 */
	setValidatorSearchForm : function(selector) {
		validatorSearch = $(selector).validate({
			rules:{
				'phone':{
					maxlength: 15,
					number: true,
					required: true
				}
			},
			messages:{
				'phone':{
					maxlength:'Por favor, introduzca un maximo de 15 caracteres.',
					number:'Por favor introduzca un numero',
					required:'Este campo es obligatorio'
				}
			}
		});
	},

	/**
	 * Sets url for action side server
	 * @param url json
	 */
	setUrl: function(url) {
		this.url = url;
	},

	/**
	 * Shows alert if it exists, if it not create a new instance of alert and show it
	 * @param message to show
	 * @param header of the message
	 */
	showAlert: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.show(message, header);
	}},

	/**
	 * Shows flash message success if it exists, if it not creates a new instance of flash message success and shows it.
	 * @param message string
	 * @param header string
	 */
	flashSuccess: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.flashSuccess(message, header);
	}},

	/**
	 * Shows flash message error if it exists, if it not creates a new instance of flash message error and shows it.
	 * @param message string
	 * @param header string
	 */
	flashError: function(message, header) {with (this) {
		if (this.alert == undefined) {
			this.alert = new com.em.Alert();
		}
		alert.flashError(message, header);
	}}
};