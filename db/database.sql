-- #timestamp <20140316235500>

CREATE  TABLE IF NOT EXISTS `tblRole` (
	`id`			INT				NOT NULL AUTO_INCREMENT,
	`name`			VARCHAR(50)		NOT NULL,
	`description`	TEXT			NULL,
	`created` 		DATETIME 		NOT NULL,
	`changed` 		DATETIME 		DEFAULT NULL,
	`createdBy` 	INT(11) 		DEFAULT NULL,
	`changedBy` 	INT(11) 		DEFAULT NULL,
	`state` 		TINYINT(1) 		NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblRole`
		PRIMARY KEY (`id`),

	KEY `i_tblRole_id` (`id`),
	INDEX `i_tblRole_state_id` (`state`, `id`))
ENGINE = INNODB;


CREATE  TABLE IF NOT EXISTS `tblResource` (
	`id`			INT				NOT NULL AUTO_INCREMENT,
	`title`			VARCHAR(50)		NOT NULL,
	`description`	TEXT			NULL,
	`created`		DATETIME		NOT NULL,
	`changed`		DATETIME		DEFAULT NULL,
	`createdBy`		INT(11)			DEFAULT NULL,
	`changedBy`		INT(11)			DEFAULT NULL,
	`state`			TINYINT(1)		NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblResource`
		PRIMARY KEY (`id`),

	KEY `i_tblResource_id` (`id`),
	INDEX `i_tblResource_state_id` (`state`, `id`))
ENGINE = INNODB;


CREATE  TABLE IF NOT EXISTS `tblRole_Resource` (
	`roleId` 		INT NOT NULL,
	`resourceId`	INT NOT NULL,
	PRIMARY KEY (`roleId`, `resourceId`),
	INDEX `fk_tblRole_Resource_roleId` (`roleId`),
	INDEX `fk_tblRole_Resource_resourceId` (`resourceId`),
	CONSTRAINT `fk_tblRole_Resource_roleId`
	FOREIGN KEY (`roleId` )
	REFERENCES `tblRole` (`id` )
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	CONSTRAINT `fk_tblRole_Resource_resourceId`
	FOREIGN KEY (`resourceId` )
	REFERENCES `tblResource` (`id` )
	ON DELETE NO ACTION
	ON UPDATE CASCADE
	)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `tblDataVault` (
	`id`		INT(11) NOT NULL AUTO_INCREMENT,
	`expires`	DATETIME DEFAULT NULL,
	`filename`	VARCHAR(150) NOT NULL,
	`mimeType`	VARCHAR(45) DEFAULT NULL,
	`binary`	LONGBLOB,
	`created`	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`state`		TINYINT(1) NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblDataVault` PRIMARY KEY (`id`),
	KEY `i_tblDataVault_id` (`id`),
	INDEX `i_tblDataVault_state_id` (`state`,`id`)
) ENGINE=INNODB;


CREATE TABLE `tblPerson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identityCard` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `phonework` varchar(45) DEFAULT NULL,
  `phonemobil` int(11) DEFAULT NULL,
  `sex` tinyint(4) NOT NULL,
  `type` varchar(15) NOT NULL,
  `created` datetime NOT NULL,
  `changed` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `changedBy` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `profilePictureId` int(11) DEFAULT NULL,
  CONSTRAINT `pk_tblPerson`
  PRIMARY KEY (`id`),
  KEY `i_tblPerson_id` (`id`),
  KEY `i_tblPerson_state_id` (`state`,`id`),
  KEY `i_tblPerson_profilePictureId` (`profilePictureId`),
  CONSTRAINT `fk_tblPerson_profilePictureId` FOREIGN KEY (`profilePictureId`) REFERENCES `tblDataVault` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `tblAccount` (
	`id`			INT(11) 		NOT NULL AUTO_INCREMENT,
	`username`		VARCHAR(45) 	NOT NULL,
	`password`		VARCHAR(128)	NOT NULL,
	`email`			VARCHAR(65)		DEFAULT NULL,
	`role`			VARCHAR(45)		DEFAULT NULL,
	`accountType`	INT(11) 		DEFAULT NULL,
	`created` 		DATETIME 		NOT NULL,
	`changed` 		DATETIME		DEFAULT NULL,
	`createdBy`		INT(11) 		DEFAULT NULL,
	`changedBy`		INT(11)			DEFAULT NULL,
	`state`			TINYINT(1) 		NOT NULL DEFAULT '1',
	
	CONSTRAINT `pk_tblAccount` PRIMARY KEY (`id`),
	KEY `i_tblAccount_id` (`id`),
	INDEX `i_tblAccount_state_id` (`state`,`id`)
) ENGINE=INNODB;


CREATE  TABLE IF NOT EXISTS `tblOperator` (
	`id` 			INT(11) 	NOT NULL AUTO_INCREMENT,
	`visible`		TINYINT(1)	NOT NULL DEFAULT '0',
	`accountId` 	INT(11)		DEFAULT NULL,
	`roleId`		INT(11)		DEFAULT NULL,

	CONSTRAINT `pk_tblOperator`
		PRIMARY KEY (`id`),

	KEY `i_tblOperator_id` (`id`),
	INDEX `i_tblOperator_accountId` (`accountId`),
	INDEX `i_tblOperator_roleId` (`roleId`),

	CONSTRAINT `fk_tblOperator_accountId`
	FOREIGN KEY (`accountId`)
	REFERENCES `tblAccount` (`id`)
	ON UPDATE CASCADE,
	CONSTRAINT `fk_tblOperator_roleId`
	FOREIGN KEY (`roleId`)
	REFERENCES `tblRole` (`id`)
	ON UPDATE CASCADE
) ENGINE = INNODB;


CREATE  TABLE IF NOT EXISTS `tblPassenger` (
	`id` 			INT(11) 	NOT NULL AUTO_INCREMENT,
	`address`		TEXT 		NULL,
	`description`	TEXT		NULL,

	CONSTRAINT `pk_tblPassenger`
		PRIMARY KEY (`id`),

	KEY `i_tblPassenger_id` (`id`)
) ENGINE = INNODB;


CREATE TABLE `tblTaxi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `mark` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `model` int(11) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `plaque` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `pictureId` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `changed` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `changedBy` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',

  CONSTRAINT `pk_tblTaxi`
  PRIMARY KEY (`id`),
  KEY `i_tblTaxi_id` (`id`),
  KEY `i_tblTaxi_state_id` (`state`,`id`),
  KEY `i_tblTaxi_pictureId` (`pictureId`),
  CONSTRAINT `fk_tblTaxi_pictureId` FOREIGN KEY (`pictureId`) REFERENCES `tblDataVault` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB;


CREATE  TABLE IF NOT EXISTS `tblDriver` (
	`id` 		INT(11) 	NOT NULL AUTO_INCREMENT,
	`address`	TEXT 		NULL,
	`note`		TEXT		NULL,
	`taxiId`	INT(11)		DEFAULT NULL,

	CONSTRAINT `pk_tblDriver`
		PRIMARY KEY (`id`),

	KEY `i_tblDriver_id` (`id`),
	INDEX `i_tblDriver_taxiId` (`taxiId`),
	
	CONSTRAINT `fk_tblDriver_taxiId`
	FOREIGN KEY (`taxiId`)
	REFERENCES `tblTaxi` (`id`)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
) ENGINE = INNODB;


-- #timestamp <20140317235500>

INSERT INTO `tblRole` ( `name`, `description`, `created`, `changed`, `createdBy`, `changedBy`, `state`) VALUES
	('Administrador', 'Encargado de Administrador todo el Sistema', NOW(), NULL, 1, NULL, 1),
	('Operador', 'Encargado de utilizar algunas funcionalidades del sistema', NOW(), NULL, 1, NULL, 1),
	('Invitado', 'No realiza ninguna funcionalidad en el Sistema', NOW(), NULL, 1, NULL, 1);
	
INSERT INTO `tblPerson`(`identityCard`,`firstName`,`lastName`,`dateOfBirth`,`phone`,`phonework`,`phonemobil`,`sex`,`type`,`created`,`changed`,`createdBy`,`changedBy`,`state`,`profilePictureId`) VALUES
	(5938782,'Victor','Villca',NOW(),'',NULL,70016783,1,'operator',NOW(),NULL,NULL,NULL,1,NULL);
INSERT INTO `tblAccount`(`username`,`password`,`email`,`role`,`accountType`,`created`,`changed`,`createdBy`,`changedBy`,`state`) VALUES
	('admin',md5('admin123456'),'victor.villca@gmail.com','Operador',1,NOW(),NULL,NULL,NULL,1);
INSERT INTO `tblOperator`(`visible`,`accountId`,`roleId`) VALUES
	(1,1,1);


-- #timestamp <20140321045500>

CREATE  TABLE IF NOT EXISTS `tblLabel` (
	`id`			INT				NOT NULL AUTO_INCREMENT,
	`name`			VARCHAR(50)		NOT NULL,
	`description`	TEXT			NULL,
	`created` 		DATETIME 		NOT NULL,
	`changed` 		DATETIME 		DEFAULT NULL,
	`createdBy` 	INT(11) 		DEFAULT NULL,
	`changedBy` 	INT(11) 		DEFAULT NULL,
	`state` 		TINYINT(1) 		NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblLabel`
		PRIMARY KEY (`id`),

	KEY `i_tblLabel_id` (`id`),
	INDEX `i_tblLabel_state_id` (`state`, `id`))
ENGINE = INNODB;


CREATE  TABLE IF NOT EXISTS `tblAddress` (
	`id`			INT				NOT NULL AUTO_INCREMENT,
	`name`			TEXT 			NOT NULL,
	`labelId`		INT(11)			DEFAULT NULL,
	`passengerId`	INT(11)			DEFAULT NULL,
	`created`		DATETIME		NOT NULL,
	`changed`		DATETIME		DEFAULT NULL,
	`createdBy`		INT(11)			DEFAULT NULL,
	`changedBy`		INT(11)			DEFAULT NULL,
	`state`			TINYINT(1)		NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblAddress`
		PRIMARY KEY (`id`),

	KEY `i_tblAddress_id` (`id`),
	INDEX `i_tblAddress_state_id` (`state`, `id`),
	INDEX `i_tblAddress_labelId` (`labelId`),
	INDEX `i_tblAddress_passengerId` (`passengerId`),

	CONSTRAINT `fk_tblAddress_labelId`
	FOREIGN KEY (`labelId`)
	REFERENCES `tblLabel` (`id`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	CONSTRAINT `fk_tblAddress_passengerId`
	FOREIGN KEY (`passengerId`)
	REFERENCES `tblPassenger` (`id`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
) ENGINE = INNODB;


-- #timestamp <20140324181100>

INSERT INTO `tblLabel` ( `name`, `description`, `created`, `changed`, `createdBy`, `changedBy`, `state`) VALUES
	('Casa', 'La Etiqueta corresponde a la Casa del Pasajero', NOW(), NULL, 1, NULL, 1),
	('Trabajo', 'La Etiqueta corresponde a la Casa del Pasajero', NOW(), NULL, 1, NULL, 1),
	('Gimnasio', 'La Etiqueta corresponde al Gimnasio del Pasajero', NOW(), NULL, 1, NULL, 1),
	('Tienda Comercial', 'La Etiqueta corresponde a la Tienda Comercial del Pasajero', NOW(), NULL, 1, NULL, 1);
	
INSERT INTO `tblPerson`(`identityCard`,`firstName`,`lastName`,`dateOfBirth`,`phone`,`phonework`,`phonemobil`,`sex`,`type`,`created`,`changed`,`createdBy`,`changedBy`,`state`,`profilePictureId`) VALUES
	(59387823,'Adan','Condori',NOW(),'700167834',NULL,700167834,1,'passenger',NOW(),NULL,NULL,NULL,1,NULL);
	
INSERT INTO `tblPassenger` ( `id`, `address`, `description`) VALUES
	(2, 'Direccion del Pasajero Victor', 'Urbanizacion Los Penocos');
	
INSERT INTO `tblAddress`(`name`, `labelId`, `passengerId`, `created`, `changed`, `createdBy`, `changedBy`, `state`) VALUES
	('Urbanizacion los Penocos', 1, 2, NOW(), NULL, 1, NULL, 1),
	('Roca y Coronado', 2, 2, NOW(), NULL, 1, NULL, 1),
	('Master Gym', 3, 2, NOW(), NULL, 1, NULL, 1);


-- #timestamp <20140325233900>

CREATE  TABLE IF NOT EXISTS `tblRide` (
	`id`			INT				NOT NULL AUTO_INCREMENT,
	`note`			TEXT 			NOT NULL,
	`notAssignedTime`	INT(11)		DEFAULT NULL,
	`ongoingTime`	INT(11)			DEFAULT NULL,
	`status`		TINYINT			DEFAULT 0,
	`passengerId`	INT(11)			DEFAULT NULL,
	`taxiId`		INT(11)			DEFAULT NULL,
	`created`		DATETIME		NOT NULL,
	`changed`		DATETIME		DEFAULT NULL,
	`createdBy`		INT(11)			DEFAULT NULL,
	`changedBy`		INT(11)			DEFAULT NULL,
	`state`			TINYINT(1)		NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblRide`
		PRIMARY KEY (`id`),

	KEY `i_tblRide_id` (`id`),
	INDEX `i_tblRide_state_id` (`state`, `id`),
	INDEX `i_tblRide_taxiId` (`taxiId`),
	INDEX `i_tblRide_passengerId` (`passengerId`),

	CONSTRAINT `fk_tblRide_taxiId`
	FOREIGN KEY (`taxiId`)
	REFERENCES `tblTaxi` (`id`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,
	CONSTRAINT `fk_tblRide_passengerId`
	FOREIGN KEY (`passengerId`)
	REFERENCES `tblPassenger` (`id`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
) ENGINE = INNODB;


-- #timestamp <20140401140800>

ALTER TABLE `tblRide`
	ADD COLUMN `labelId`		INT(11)		DEFAULT NULL,

	ADD INDEX `i_tblRide_labelId` (`labelId`),
	ADD CONSTRAINT `fk_tblRide_labelId`
		FOREIGN KEY (`labelId`)
		REFERENCES `tblLabel` (`id`)
		ON DELETE RESTRICT
		ON UPDATE CASCADE;
		
		
ALTER TABLE `tblTaxi`
	ADD COLUMN `number`		INT(11)		NOT NULL DEFAULT 0;


-- #timestamp <20140415051800>

CREATE  TABLE IF NOT EXISTS `tblBacktrack` (
	`id`			INT  		NOT NULL AUTO_INCREMENT,
	`taxiId`			INT  		NOT NULL,
	`latitud`			VARCHAR(50)	NOT NULL,
	`longitud`			VARCHAR(50)	NOT NULL,
	`status`			VARCHAR(10)	NOT NULL,
	`timenow`			DATETIME 	DEFAULT NULL,
	`created` 		DATETIME 	NOT NULL,
	`changed` 		DATETIME 	DEFAULT NULL,
	`createdBy` 	INT(11) 		DEFAULT NULL,
	`changedBy` 	INT(11) 		DEFAULT NULL,
	`state` 		TINYINT(1) 	NOT NULL DEFAULT '1',
	CONSTRAINT `pk_tblBacktrack`
		PRIMARY KEY (`id`),

	KEY `i_tblBacktrack_id` (`id`),
	INDEX `i_tblBacktrack_state_id` (`state`, `id`),
	INDEX `i_tblBacktrack_taxiId` (`taxiId`),

	CONSTRAINT `fk_tblBacktrack_taxiId`
	FOREIGN KEY (`taxiId`)
	REFERENCES `tblTaxi` (`id`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
) ENGINE = INNODB;


-- #timestamp <20140417094100>

ALTER TABLE `tblTaxi`
	ADD COLUMN `active`		BOOL		NOT NULL DEFAULT 0;

ALTER TABLE `tblTaxi`
	ADD COLUMN `phone` VARCHAR(15) DEFAULT NULL,
	ADD COLUMN `codeactivation` VARCHAR(50) DEFAULT NULL,
	ADD COLUMN `codeuser` VARCHAR(10) DEFAULT NULL
	;

ALTER TABLE `tblTaxi`
	ADD COLUMN `activeimage`	BOOL	NOT NULL DEFAULT 0;
	
ALTER TABLE `tblTaxi`
	ADD COLUMN `dateStatus` DATETIME DEFAULT NULL;

ALTER TABLE `tblRide`
	ADD COLUMN `dateStatus` DATETIME DEFAULT NULL;