<?php
/**
 * Validate user session.
 *
 * @category Taxi Director
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 LeaderSoft A/S
 * @license Proprietary
 * @package Dis
 */

class Dis_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract {

    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Plugin_Abstract::preDispatch()
     */
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
	    $auth = Zend_Auth::getInstance();

        if ($request->getModuleName() == 'default') {
            return TRUE;
        }

        if($auth->hasIdentity()) {
            return TRUE;
        } else {
            $request
                ->setModuleName('user')
                ->setControllerName('Auth')
            ;
            return FALSE;
        }
    }
}