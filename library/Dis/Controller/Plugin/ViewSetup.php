<?php

require_once ('Zend/Controller/Plugin/Abstract.php');

class Dis_Controller_Plugin_ViewSetup extends Zend_Controller_Plugin_Abstract {

	/**
	 * (non-PHPdoc)
	 * @see Zend_Controller_Plugin_Abstract::postDispatch()
	 */
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
		// if not the request has been dispatched
		if (!$request->isDispatched()) {
			return;
		}
		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$view = $bootstrap->getResource('view');

		if ( $view->title ) {
			$view->headTitle($view->title);
			$view->headTitle()->setSeparator(' - ');
		}

		$moduleName = $request->getModuleName();
		if ($moduleName === "default") {
			//if module default use the template
			return;
		}

		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		if (isset($config[$moduleName]['resources']['layout']['layout'])) {
            $layoutScript = $config[$moduleName]['resources']['layout']['layout'];
            Zend_Layout::getMvcInstance()->setLayout($layoutScript);
        }

        if (isset($config[$moduleName]['resources']['layout']['layoutPath'])) {
            $layoutPath = $config[$moduleName]['resources']['layout']['layoutPath'];
            $moduleDir = Zend_Controller_Front::getInstance()->getModuleDirectory();
            Zend_Layout::getMvcInstance()->setLayoutPath($layoutPath);
        }
	}
}