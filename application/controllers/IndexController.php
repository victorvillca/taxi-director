<?php
/**
 * Controller for Taxi Director.
 *
 * @category Taxi
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 LeaderSoft A/S
 * @license Proprietary
 */

class IndexController extends Dis_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    /**
     * This action redirects to Login Page in the module admin
     */
    public function indexAction() {

    	/*$params=array(
			'host' =>'localhost',
            'username' =>'root',
            'password' =>'',
            'dbname' =>'dbtaxi');*/

        if ((isset($_GET['modo']) && $_GET['modo'] != '') && (isset($_GET['msg']) && $_GET['msg'] != '')) {

    		$modo = $_GET['modo'];

    		// response Array
    		$response = array("ok" => "1");//en  "OK" VA NRO MAYOR A "0" SI TODO SALIO CON EXITO 
								   //CASO CONTRARIO MENOR 
								   //ESTE VALOR SIEMPRE TIENE QUE IR EN CUALQUIER RESPUESTA
								   //LUEGO SE LE AGREGA AL MISMO ARRAY LO K QUIERAS 
		    switch ($modo) {
		        case 'nombre':

					//AQUI DEBES IMPLEMENTAR TU CONEXION A TU BD Y DEVOLVER EN "$response" EN FORMATO JSON			
		            
		            $this->_helper->json($response);
		            break;
		        case 'distrito':
		        	$params=array(
						'host' =>'localhost',
			            'username' =>'sperey_victor',
			            'password' =>'vrvv5938782vsq',
			            'dbname' =>'sperey_taxi_zf');

			    	$db = Zend_Db::Factory('PDO_MYSQL',$params);

		            $namechurch = $_GET['msg'];

					//SELECT NAME, description, latitude, longitude FROM tblChurch WHERE NAME LIKE '%nuevo%' AND districtId = 9//example Nuevo Palmar
//		            $sql = "SELECT name, description, latitude, longitude FROM tblChurch WHERE NAME LIKE '%".$namechurch."%' AND districtId = $districtId";

		            $sql = "SELECT name, description, latitude, longitude FROM tblChurch WHERE NAME LIKE '%".$namechurch."%'";
    				$result = $db->fetchAll($sql);

		            $this->_helper->json($result);
		            break;
		        case 'listadistritos':
		            //AQUI DEBES IMPLEMENTAR TU CONEXION A TU BD Y DEVOLVER EN "$response" EN FORMATO JSON
		            
		            $this->_helper->json($response);
		            break;
		    }
		}
    }
}