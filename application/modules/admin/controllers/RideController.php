<?php
/**
 * Controller for DIST 3.
 *
 * @category Dist
 * @author Victor Villca <victor.villca@people-trust.com>
 * @copyright Copyright (c) 2013 Gisof A/S
 * @license Proprietary
 */

use Model\Passenger;
use Model\Address;
use Model\Taxi;
use Model\Ride;
use Model\Person;

class Admin_RideController extends Dis_Controller_Action {

	/**
	 * (non-PHPdoc)
	 * @see App_Controller_Action::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * This action shows the sama page initial but swith functionalities
	 * @access public
	 */
	public function indexAction() {
	    $this->_helper->redirector('Index', 'admin', array());
	}

	/**
	 * This action shows the form to save the Ride
	 * @access public
	 */
	public function addAction() {
		$this->_helper->layout()->disableLayout();

		$labelRepo = $this->_entityManager->getRepository('Model\Label');
		$taxiRepo = $this->_entityManager->getRepository('Model\Taxi');
		$taxisArray = $taxiRepo->findByStatusArrayNumber(Taxi::WITHOUT_CAREER);

		$form = new Dis_Form_Ride();
		$form->getElement('taxi')->setMultiOptions($taxisArray);
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());

		$this->view->form = $form;
	}

	/**
	 * Saves the new Ride of the passanger
	 * @access public
	 */
	public function addSaveAction() {
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$labelRepo = $this->_entityManager->getRepository('Model\Label');
		$taxiRepo = $this->_entityManager->getRepository('Model\Taxi');

		$form = new Dis_Form_Ride();
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());
		$form->getElement('taxi')->setMultiOptions($taxiRepo->findByStatusArrayNumber(Taxi::WITHOUT_CAREER));

        $formData = $this->getRequest()->getPost();
        if ($form->isValid($formData)) {
            $passengerRepo = $this->_entityManager->getRepository('Model\Passenger');
            if ($passengerRepo->verifyExistPhone($formData['phone'])) {
                $label = $this->_entityManager->find('Model\Label', (int)$formData['label']);
                $passenger = $this->_entityManager->find('Model\Passenger', (int)$formData['id']);
                $taxi = $this->_entityManager->find('Model\Taxi', (int)$formData['number']);

                $addressRepo = $this->_entityManager->getRepository('Model\Address');
                $address = $addressRepo->findByPassengerAndLabel($passenger, $label);
                $address->setName($formData['address']);

                $this->_entityManager->persist($address);
                $this->_entityManager->flush();

                $ride = new Ride();
                $ride
                    ->setDateStatus(new DateTime('now'))
                    ->setLabel($label)
                    ->setPassenger($passenger)
                    ->setNotAssignedTime(1)
                    ->setNote($formData['note'])
                    ->setState(TRUE)
                    ->setCreated(new DateTime('now'))
                ;
                if ($taxi != NULL) {
                	$ride->setStatus(Ride::ONGOING);
                	$taxi->setStatus(Taxi::ONGOING);
                	$ride->setTaxi($taxi);
                } else {
                    $ride->setStatus(Ride::NOT_ASSIGNED);
                }

                $this->_entityManager->persist($ride);
                $this->_entityManager->flush();

                $this->stdResponse = new stdClass();
				$this->stdResponse->success = TRUE;
				$this->stdResponse->message = _('Carrera registrado');
			} else {
			    $passenger = new Passenger();
			    $passenger
                    ->setSex(Person::SEX_DEFAULT)
                    ->setLastName(Person::LAST_NAME_DEFAULT)
                    ->setAddress($formData['address'])
                    ->setDateOfBirth(new DateTime('now'))
                    ->setCreated(new DateTime('now'))
                    ->setState(TRUE)
                    ->setIdentityCard(Person::IDENTITY_CARD)
                    ->setPhone($formData['phone'])
                    ->setFirstName($formData['name'])
                ;

                $this->_entityManager->persist($passenger);
                $this->_entityManager->flush();

                $label = $this->_entityManager->find('Model\Label', (int)$formData['label']);

                $address = new Address();
                $address
                    ->setName($formData['address'])
                    ->setPassenger($passenger)
                    ->setLabel($label)
                    ->setState(TRUE)
                    ->setCreated(new DateTime('now'))
                ;

                $this->_entityManager->persist($address);
                $this->_entityManager->flush();

                $taxi = $this->_entityManager->find('Model\Taxi', (int)$formData['number']);

                $ride = new Ride();
                $ride
                    ->setDateStatus(new DateTime('now'))
                    ->setLabel($label)
                    ->setPassenger($passenger)
                    ->setNotAssignedTime(1)
                    ->setNote($formData['note'])
                    ->setState(TRUE)
                    ->setCreated(new DateTime('now'))
			    ;

			    if ($taxi != NULL) {
			    	$ride->setStatus(Ride::ONGOING);
			    	$taxi->setStatus(Taxi::ONGOING);
			    	$ride->setTaxi($taxi);
			    } else {
			    	$ride->setStatus(Ride::NOT_ASSIGNED);
			    }

			    $this->_entityManager->persist($ride);
			    $this->_entityManager->flush();

				$this->stdResponse = new stdClass();
				$this->stdResponse->success = TRUE;
				$this->stdResponse->message = _('Carrera registrado');
			}
		} else {
            $this->stdResponse = new stdClass();
			$this->stdResponse->success = FALSE;
			$this->stdResponse->messageArray = $form->getMessages();
			$this->stdResponse->message = _('El Formulario tiene Errores');
		}
		// sends response to client
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * This action shows a form to search passengers
	 * @access public
	 */
	public function searchAction() {
		$this->_helper->layout()->disableLayout();

		$labelRepo = $this->_entityManager->getRepository('Model\Label');

		$form = new Dis_Form_SearchPassenger();
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());

		$this->view->form = $form;
	}

	/**
	 * Searchs the Passenger by number phone
	 * @access public
	 */
	public function dsSearchAction() {
	    $this->_helper->viewRenderer->setNoRender(TRUE);

        $phone = $this->_getParam('phone', NULL);

	    $passengerRepo = $this->_entityManager->getRepository('Model\Passenger');
	    $passenger = $passengerRepo->findByPhone($phone);

	    $data = NULL;
	    if ($passenger != NULL) {
	        $addressRepo = $this->_entityManager->getRepository('Model\Address');
	        $addresses = $addressRepo->findByPassenger($passenger);

	        $addressArray = array();
	        $addressName = '';
	        $swName = TRUE;
	        foreach ($addresses as $address) {
	            if ($swName) {
                    $addressName = $address->getName();
                    $swName = FALSE;
	            }
                $label = $address->getLabel();
                if ($label != NULL) {
                	$addressArray[$label->getId()] = $label->getName();
                }
	        }
            $data = array(
                'id' => $passenger->getId(),
                'name' => $passenger->getFirstName(),
                'address' => $addressName
            );

            $this->stdResponse = new stdClass();
            $this->stdResponse->success = TRUE;
            $this->stdResponse->addressArray = $addressArray;
	    } else {
	        $labelRepo = $this->_entityManager->getRepository('Model\Label');

	        $this->stdResponse = new stdClass();
	        $this->stdResponse->success = FALSE;
	        $this->stdResponse->addressArray = $labelRepo->findAllArray();
	    }

	    $this->stdResponse->data = $data;
	    $this->_helper->json($this->stdResponse);
	}

	/**
	 * This action shows the form to update the Ride.
	 * @access public
	 */
	public function editAction() {
		$this->_helper->layout()->disableLayout();

		$form = new Dis_Form_Ride();
		$form->setOnlyRead(TRUE);

		$id = $this->_getParam('id', 0);
		$ride = $this->_entityManager->find('Model\Ride', $id);
		if ($ride != NULL) {//security
            $passenger = $ride->getPassenger();
		    $phone = $passenger->getPhone();
		    $name = $passenger->getFirstName();
		    $label = $ride->getLabel();

		    $addressRepo = $this->_entityManager->getRepository('Model\Address');
		    $address = $addressRepo->findByPassengerAndLabel($passenger, $label);

		    $form->getElement('id')->setValue($ride->getId());
		    $form->getElement('phone')->setValue($phone);
		    $form->getElement('name')->setValue($name);

            $form->getElement('phone')->setRequired(FALSE);
            $form->getElement('name')->setRequired(FALSE);

		    $taxiRepo = $this->_entityManager->getRepository('Model\Taxi');

		    $taxisArray = $taxiRepo->findByStatusArrayNumber(Taxi::WITHOUT_CAREER);

            $form
                ->setPhone($phone)
                ->setName($name)
                ->setLabel($label->getName())
                ->setAddress($address->getName())
                ->setNote($ride->getNote())
            ;

            $form->getElement('taxi')->setMultiOptions($taxisArray);
		} else {
			// response to client
            $this->stdResponse = new stdClass();
			$this->stdResponse->success = FALSE;
			$this->stdResponse->message = _('The requested record was not found.');
			$this->_helper->json($this->stdResponse);
		}

		$this->view->form = $form;
	}

	/**
	 * This action shows the resume of the Ride on going.
	 * @access public
	 */
	public function resumeAction() {
		$this->_helper->layout()->disableLayout();
		$form = new Dis_Form_Ride();
		$form->setOnlyRead(TRUE);
		$form->setIsResume(TRUE);

		$id = $this->_getParam('id', 0);
		$ride = $this->_entityManager->find('Model\Ride', $id);
		if ($ride != NULL) {//security
			$passenger = $ride->getPassenger();
			$label = $ride->getLabel();
			$taxi = $ride->getTaxi();

			$addressRepo = $this->_entityManager->getRepository('Model\Address');
			$address = $addressRepo->findByPassengerAndLabel($passenger, $label);

			$form->getElement('phone')->setValue($passenger->getPhone());
			$form->getElement('name')->setValue($passenger->getFirstName());

			$form
    			->setPhone($passenger->getPhone())
    			->setName($passenger->getFirstName())
    			->setLabel($label->getName())
    			->setAddress($address->getName())
    			->setNote($ride->getNote())
    			->setTaxi($taxi->getNumber())
			;
		} else {
			// response to client
			$this->stdResponse = new stdClass();
			$this->stdResponse->success = FALSE;
			$this->stdResponse->message = _('The requested record was not found.');
			$this->_helper->json($this->stdResponse);
		}

		$this->view->form = $form;
	}

	/**
	 * Updates Ride not assigned
	 * @access public
	 */
	public function editSaveAction() {
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$taxiRepo = $this->_entityManager->getRepository('Model\Taxi');
		$taxisArray = $taxiRepo->findByStatusArrayNumber(Taxi::WITHOUT_CAREER);

		$form = new Dis_Form_Ride();
		$form->setOnlyRead(TRUE);
		$form->getElement('phone')->setRequired(FALSE);
		$form->getElement('name')->setRequired(FALSE);
		$form->getElement('label')->setRequired(FALSE);
		$form->getElement('address')->setRequired(FALSE);
		$form->getElement('taxi')->setMultiOptions($taxisArray);

		$formData = $this->getRequest()->getPost();
		if ($form->isValid($formData)) {
			$id = $this->_getParam('id', 0);
			$ride = $this->_entityManager->find('Model\Ride', $id);
			if ($ride != NULL) {
                $taxi = $this->_entityManager->find('Model\Taxi', (int)$formData['taxi']);
                $ride->setChanged(new DateTime('now'));

                if ($taxi != NULL) {
                    $ride->setStatus(Ride::ONGOING);
                    $taxi->setStatus(Taxi::ONGOING);
                    $ride->setTaxi($taxi);
                    $ride->setDateStatus(new DateTime('now'));
                }

				$this->_entityManager->persist($ride);
				$this->_entityManager->flush();

				$this->stdResponse = new stdClass();
				$this->stdResponse->success = TRUE;
				$this->stdResponse->message = _('Carrera Actualizada');
			} else {
                $this->stdResponse = new stdClass();
				$this->stdResponse->success = FALSE;
				$this->stdResponse->message = _('No existe la Carrera');
			}
		} else {
            $this->stdResponse = new stdClass();
            $this->stdResponse->success = FALSE;
			$this->stdResponse->messageArray = $form->getMessages();
			$this->stdResponse->message = _('El Formulario contiene Errores');
		}
		// sends response to client
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Outputs an XHR response containing all entries the rides not assigned.
	 * @xhrParam int filter_name
	 * @xhrParam int iDisplayStart
	 * @xhrParam int iDisplayLength
	 */
	public function dsRideEntriesNotAssignedAction() {
		$filterParams['name'] = $this->_getParam('filter_name', NULL);

		$filters = array();
		$filters[] = array('field' => 'status', 'filter' => Ride::NOT_ASSIGNED, 'operator' => '=');

		$start = $this->_getParam('iDisplayStart', 0);
		$limit = $this->_getParam('iDisplayLength', 10);
		$page = ($start + $limit) / $limit;

		$rideRepo = $this->_entityManager->getRepository('Model\Ride');
		$rides = $rideRepo->findByCriteria($filters, $limit, $start);
		$total = $rideRepo->getTotalCount($filters);

		$posRecord = $start+1;
		$data = array();
		foreach ($rides as $ride) {
		    $timeText = '(0 min)';
		    if ($ride->getDateStatus() != NULL) {
// 		    	$backtracks = $backtrackRepo->findByTaxiAndStatusAndDateStatus($taxi, Taxi::WITHOUT_CAREER, $taxi->getDateStatus());
// 		    	if (count($backtracks) > 0) {
		    		$timenow = $ride->getDateStatus();
		    		$dateCurrent = new DateTime('now');
		    		$interval = $dateCurrent->diff($timenow);
// 		    		$timeText = '(' . $interval->format('%d %h:%i:%s') . ' min)';
		    		$timeText = '(' . $interval->format('%i') . ' min)';
// 		    	}
		    }

			$passenger = $ride->getPassenger();

			$row = array();
			$row[] = $ride->getId();
			$row[] = $passenger->getPhone().' '. $timeText;;
			$data[] = $row;
			$posRecord++;
		}
		// response
		$this->stdResponse = new stdClass();
		$this->stdResponse->iTotalRecords = $total;
		$this->stdResponse->iTotalDisplayRecords = $total;
		$this->stdResponse->aaData = $data;
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Outputs an XHR response containing all entries the rides on going.
	 * @xhrParam int filter_name
	 * @xhrParam int iDisplayStart
	 * @xhrParam int iDisplayLength
	 */
	public function dsRideEntriesOnGoingAction() {
		$filterParams['name'] = $this->_getParam('filter_name', NULL);

		$filters = array();
		$filters[] = array('field' => 'status', 'filter' => Ride::ONGOING, 'operator' => '=');

		$start = $this->_getParam('iDisplayStart', 0);
		$limit = $this->_getParam('iDisplayLength', 10);
		$page = ($start + $limit) / $limit;

		$rideRepo = $this->_entityManager->getRepository('Model\Ride');
		$rides = $rideRepo->findByCriteria($filters, $limit, $start);
		$total = $rideRepo->getTotalCount($filters);

		$posRecord = $start+1;
		$data = array();
		foreach ($rides as $ride) {
		 $timeText = '(0 min)';
		    if ($ride->getDateStatus() != NULL) {
// 		    	$backtracks = $backtrackRepo->findByTaxiAndStatusAndDateStatus($taxi, Taxi::WITHOUT_CAREER, $taxi->getDateStatus());
// 		    	if (count($backtracks) > 0) {
		    		$timenow = $ride->getDateStatus();
		    		$dateCurrent = new DateTime('now');
		    		$interval = $dateCurrent->diff($timenow);
		    		$timeText = '(' . $interval->format('%d %h:%i:%s') . ' min)';
// 		    	}
		    }

			$passenger = $ride->getPassenger();

			$row = array();
			$row[] = $ride->getId();
			$row[] = $passenger->getPhone().' '. $timeText;;
			$data[] = $row;
			$posRecord++;
		}
		// response
		$this->stdResponse = new stdClass();
		$this->stdResponse->iTotalRecords = $total;
		$this->stdResponse->iTotalDisplayRecords = $total;
		$this->stdResponse->aaData = $data;
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Outputs an XHR response containing all positions the taxis.
	 * @xhrParam int status
	 * @access public
	 */
	public function dsPositionTaxisAction() {
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$status = $this->_getParam('status', -1);

		$backtrackRepo = $this->_entityManager->getRepository('Model\Backtrack');

		$filters = array();
		$filters[] = array('field' => 'status', 'filter' => $status, 'operator' => '=');

		$taxiRepo = $this->_entityManager->getRepository('Model\Taxi');
		$taxis = $taxiRepo->findByCriteria($filters);

		$data = array();
		foreach ($taxis as $taxi) {
			$route = $backtrackRepo->findLastPositionByTaxi($taxi);
			if ($route != NULL) {
				$row = array();
				$row['latitud'] = $route->getLatitud();
				$row['longitud'] = $route->getLongitud();
				$row['name'] = sprintf('Movil %d', $taxi->getNumber());
				$row['active'] = $taxi->getActiveimage();
				$data[] = $row;
			}
		}

		$this->stdResponse = new stdClass();
		$this->stdResponse = $data;
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Outputs an XHR response containing all positions the taxis.
	 * @xhrParam int status
	 * @access public
	 */
	public function dsPositionTrajectoryTaxisAction() {
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$status = $this->_getParam('status', -1);

		$backtrackRepo = $this->_entityManager->getRepository('Model\Backtrack');

		$filters = array();
		$filters[] = array('field' => 'status', 'filter' => $status, 'operator' => '=');

		$taxiRepo = $this->_entityManager->getRepository('Model\Taxi');
		$taxis = $taxiRepo->findByCriteria($filters);


		$data = array();
		foreach ($taxis as $taxi) {
			$route = $backtrackRepo->findLastPositionByTaxi($taxi);
			if ($route != NULL) {
				$row = array();
				$row['latitud'] = $route->getLatitud();
				$row['longitud'] = $route->getLongitud();
				$row['name'] = sprintf('Movil %d', $taxi->getNumber());
				$row['active'] = $taxi->getActiveimage();
				$data[] = $row;
			}
		}

		$this->stdResponse = new stdClass();
		$this->stdResponse = $data;
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Shows the view for the rides not assigned
	 */
	public function notassignedAction() {
	}

	/**
	 * Shows the view for the rides on going
	 */
	public function ongoingAction() {
	}

	/**
	 * Deletes the Ride
	 * @access public
	 */
	public function deleteAction() {
	    $this->_helper->viewRenderer->setNoRender(TRUE);

        $rideId = $this->_getParam('rideId', 0);
        $ride = $this->_entityManager->find('Model\Ride', $rideId);

	    if ($ride != NULL) {
            $ride->setChanged(new DateTime('now'));
            $ride->setState(FALSE);

            $this->_entityManager->persist($ride);
            $this->_entityManager->flush();

            $this->stdResponse = new stdClass();
            $this->stdResponse->success = TRUE;
            $this->stdResponse->message = _('Carrera Eliminada');
	    } else {
            $this->stdResponse = new stdClass();
            $this->stdResponse->success = FALSE;
            $this->stdResponse->message = _('No se pudo Eliminar la Carrera');
	    }

	    $this->_helper->json($this->stdResponse);
	}
}