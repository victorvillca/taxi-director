<?php
/**
 * Controller for DIST 3.
 *
 * @category Dist
 * @author Victor Villca <victor.villca@people-trust.com>
 * @copyright Copyright (c) 2013 Gisof A/S
 * @license Proprietary
 */

use Model\Address;
use Model\Passenger;
use Model\Person;

class Admin_PassengerController extends Dis_Controller_Action {

	/**
	 * (non-PHPdoc)
	 * @see App_Controller_Action::init()
	 */
	public function init() {
		parent::init();
	}

	/**
	 * This action shows form dialog to create passenger
	 * @access public
	 */
	public function addAction() {
		$this->_helper->layout()->disableLayout();

		$labelRepo = $this->_entityManager->getRepository('Model\Label');

		$form = new Dis_Form_Passenger();
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());

		$this->view->form = $form;
	}

	/**
	 * Creates and save a new Passenger
	 * @access public
	 */
	public function addSaveAction() {
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$labelRepo = $this->_entityManager->getRepository('Model\Label');

		$form = new Dis_Form_Passenger();
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());

		$formData = $this->getRequest()->getPost();
		if ($form->isValid($formData)) {
            $passengerRepo = $this->_entityManager->getRepository('Model\Passenger');
			if (!$passengerRepo->verifyExistPhone($formData['phone'])) {

				$passenger = new Passenger();
				$passenger
				    ->setSex(Person::SEX_MALE)
				    ->setLastName(Person::LAST_NAME_DEFAULT)
				    ->setAddress($formData['address'])
				    ->setDateOfBirth(new DateTime('now'))
				    ->setCreated(new DateTime('now'))
				    ->setState(TRUE)
				    ->setIdentityCard(Person::IDENTITY_CARD)
                    ->setPhonemobil(Person::PHONE_MOVIL)
				    ->setPhone($formData['phone'])
                    ->setFirstName($formData['firstName'])
                ;

                $this->_entityManager->persist($passenger);
                $this->_entityManager->flush();

                $label = $this->_entityManager->find('Model\Label', (int)$formData['label']);

                $address = new Address();
                $address
                    ->setName($formData['address'])
                    ->setPassenger($passenger)
                    ->setLabel($label)
                    ->setState(TRUE)
                    ->setCreated(new DateTime('now'))
                ;

                $this->_entityManager->persist($address);
                $this->_entityManager->flush();

                $this->stdResponse = new stdClass();
				$this->stdResponse->success = TRUE;
				$this->stdResponse->message = _('Pasajero registrado');
			} else {
			    $this->stdResponse = new stdClass();
				$this->stdResponse->success = FALSE;
				$this->stdResponse->phone_duplicate = TRUE;
				$this->stdResponse->message = _('El Telefono ya existe');
			}
		} else {
            $this->stdResponse = new stdClass();
			$this->stdResponse->success = FALSE;
			$this->stdResponse->messageArray = $form->getMessages();
			$this->stdResponse->message = _('The form contains error and is not saved');
		}
		// sends response to client
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * This action shows form dialog to search passenger
	 * @access public
	 */
	public function searchAction() {
		$this->_helper->layout()->disableLayout();

		$labelRepo = $this->_entityManager->getRepository('Model\Label');

		$form = new Dis_Form_SearchPassenger();
		$form->getElement('label')->setMultiOptions($labelRepo->findAllArray());

		$this->view->form = $form;
	}

	/**
	 * Searchs the Passenger by number phone
	 * @access public
	 */
	public function dsSearchAction() {
	    $this->_helper->viewRenderer->setNoRender(TRUE);

        $phone = $this->_getParam('phone', NULL);

	    $passengerRepo = $this->_entityManager->getRepository('Model\Passenger');
	    $passenger = $passengerRepo->findByPhone($phone);

	    $data = NULL;
	    if ($passenger != NULL) {
	        $addressRepo = $this->_entityManager->getRepository('Model\Address');
	        $addresses = $addressRepo->findByPassenger($passenger);

	        $addressArray = array();
	        $addressName = '';
	        $swName = TRUE;
	        foreach ($addresses as $address) {
	            if ($swName) {
                    $addressName = $address->getName();
                    $swName = FALSE;
	            }
                $label = $address->getLabel();
                if ($label != NULL) {
                	$addressArray[$label->getId()] = $label->getName();
                }
	        }
            $data = array(
                'id' => $passenger->getId(),
                'name' => $passenger->getFirstName(),
                'address' => $addressName
            );

            $this->stdResponse = new stdClass();
            $this->stdResponse->success = TRUE;
            $this->stdResponse->addressArray = $addressArray;
	    } else {
	        $this->stdResponse = new stdClass();
	        $this->stdResponse->success = FALSE;
	    }

	    $this->stdResponse->data = $data;
	    $this->_helper->json($this->stdResponse);
	}

	/**
	 * Returns an associative array where:
	 * field: name of the table field
	 * filter: value to match
	 * operator: the sql operator.
	 * @param array $filterParams contains the values selected by the user.
	 * @return array(field, filter, operator)
	 */
	private function getFilters($filterParams) {
		$filters = array ();

		if (empty($filterParams)) {
			return $filters;
		}

		if (!empty($filterParams['name'])) {
			$filters[] = array('field' => 'name', 'filter' => '%'.$filterParams['name'].'%', 'operator' => 'LIKE');
		}

		return $filters;
	}

	/**
	 * Outputs an XHR response, loads the names of passengers.
	 * @access public
	 */
	public function autocompleteLabelAction() {
		$filterParams['name'] = $this->_getParam('name_auto', NULL);
		$filters = $this->getFilters($filterParams);

		$labelRepo = $this->_entityManager->getRepository('Model\Label');
		$labels = $labelRepo->findByCriteria($filters);

		$data = array();
		foreach ($labels as $label) {
			$data[] = $label->getName();
		}

		$this->stdResponse = new stdClass();
		$this->stdResponse->items = $data;
		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Outputs an XHR response, the name address of the passengers.
	 * @access public
	 */
	public function dsPassengerAddressAction() {
	    $passengerId = (int)$this->_getParam('passengerId', 0);
        $labelId = (int)$this->_getParam('labelId', 0);

        $label = $this->_entityManager->find('Model\Label', $labelId);
	    $passenger = $this->_entityManager->find('Model\Passenger', $passengerId);

	    $nameAddress = '';
        if ($passenger != NULL) {
            $addressRepo = $this->_entityManager->getRepository('Model\Address');
            $address = $addressRepo->findByPassengerAndLabel($passenger, $label);

            if ($address != NULL) {
                $nameAddress = $address->getName();
            }
        }

        $this->stdResponse = new stdClass();
        $this->stdResponse->nameAddress = $nameAddress;

	    $this->_helper->json($this->stdResponse);
	}

	/**
	 * Changes the name for the passenger
	 * @access public
	 */
	public function dsChangeNameAction() {
		$formData = $this->_getAllParams();
		$passenger = $this->_entityManager->find('Model\Passenger', (int)$formData['id']);
		if ($passenger != NULL) {
			$passenger
                ->setFirstName($formData['firstName'])
                ->setPhone($formData['phone'])
                ->setChanged(new DateTime('now'))
			;

			$this->_entityManager->persist($passenger);
			$this->_entityManager->flush();

			$this->stdResponse = new stdClass();
			$this->stdResponse->success = TRUE;
			$this->stdResponse->message = _('Cambio realizado');
		} else {
            $this->stdResponse = new stdClass();
            $this->stdResponse->message = _('Cambio No realizado Pasajero no entrado');
            $this->stdResponse = new stdClass();
            $this->stdResponse->success = FALSE;
		}

		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Deletes label for the Address
	 * @access public
	 */
	public function dsDeleteLabelAction() {
		$formData = $this->_getAllParams();
		$passenger = $this->_entityManager->find('Model\Passenger', (int)$formData['id']);
		$label = $this->_entityManager->find('Model\Label', (int)$formData['label']);
		if ($passenger != NULL && $label != NULL) {

		    $addressRepo = $this->_entityManager->getRepository('Model\Address');
		    $address = $addressRepo->findByPassengerAndLabel($passenger, $label);
            $address
                ->setState(FALSE)
                ->setChanged(new DateTime('now'))
            ;

			$this->_entityManager->persist($address);
			$this->_entityManager->flush();

			$addresses = $addressRepo->findByPassenger($passenger);

			$addressArray = array();
			$addressName = '';
			$swName = TRUE;
			foreach ($addresses as $add) {
				if ($swName) {
					$addressName = $add->getName();
					$swName = FALSE;
				}
				$label = $add->getLabel();
				if ($label != NULL) {
					$addressArray[$label->getId()] = $label->getName();
				}
			}
			$data = array(
                'address' => $addressName
			);

			$this->stdResponse = new stdClass();
			$this->stdResponse->success = TRUE;
			$this->stdResponse->addressArray = $addressArray;
// 			$this->stdResponse->message = _('Etiqueta Eliminado');
		} else {
			$this->stdResponse = new stdClass();
			$this->stdResponse->success = FALSE;
// 			$this->stdResponse->message = _('Cambio No realizado Pasajero no entrado');
		}

		$this->_helper->json($this->stdResponse);
	}

	/**
	 * Changes the address of the passenger
	 * @access public
	 */
	public function dsChangeAddressAction() {
		$formData = $this->_getAllParams();
		$passenger = $this->_entityManager->find('Model\Passenger', (int)$formData['id']);
		$label = $this->_entityManager->find('Model\Label', (int)$formData['label']);
		if ($passenger != NULL && $label != NULL) {

			$addressRepo = $this->_entityManager->getRepository('Model\Address');
			$address = $addressRepo->findByPassengerAndLabel($passenger, $label);
			$address
    			->setName($formData['address'])
    			->setChanged(new DateTime('now'))
			;

			$this->_entityManager->persist($address);
			$this->_entityManager->flush();

			$addresses = $addressRepo->findByPassenger($passenger);

			$addressArray = array();
			$addressName = '';
			$swName = TRUE;
			foreach ($addresses as $address) {
				if ($swName) {
					$addressName = $address->getName();
					$swName = FALSE;
				}
				$label = $address->getLabel();
				if ($label != NULL) {
					$addressArray[$label->getId()] = $label->getName();
				}
			}
			$data = array(
				'address' => $addressName
			);

			$this->stdResponse = new stdClass();
			$this->stdResponse->success = TRUE;
			$this->stdResponse->message = _('Cambio realizado');
		} else {
		    $this->stdResponse = new stdClass();
		    $this->stdResponse->success = FALSE;
		    $this->stdResponse->message = _('Cambio No realizado Pasajero no entrado');
		}

		$this->_helper->json($this->stdResponse);
	}
}