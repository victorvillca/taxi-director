<?php
/**
 * Model Doctrine 2 for Taxi Director
 *
 * @category Taxi Director
 * @package Model
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 LeaderSoft A/S
 * @license Proprietary
 */

namespace Model;

/**
 * @Entity(repositoryClass="Model\Repositories\ResourceRepository")
 * @Table(name="Resource")
 */
class Resource extends DomainObject {

	/**
	 * @Column(type="string")
	 * @var string
	 */
	private $title;

	/**
	 * @Column(type="string")
	 * @var string
	 */
	private $description;

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string title
	 * @return Resource
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return Resource
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}
}