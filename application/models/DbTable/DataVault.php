<?php
/**
 * Table Model Zend for Taxi Director
 *
 * @category Taxi
 * @package Model
 * @author Victor Villca <victor.villca.v@gmail.com>
 * @copyright Copyright (c) 2014 LeaderSoft A/S
 * @license Proprietary
 */

class Dis_Model_DbTable_DataVault extends Zend_Db_Table_Abstract {
	protected $_name = 'tblDataVault';
}